from app import create_app, socketio
from flask_cors import CORS

from app.services.csv_service import CsvService

app = create_app()
CORS(app)

if __name__ == '__main__':
    socketio.run(app, use_reloader=True, log_output=True, debug=True)