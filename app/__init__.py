
from flask import Flask
from .socketio_config import socketio
from flask_jwt_extended import JWTManager

from .config import Config
from .database.db import database_config

from .views.csv_view import csv_view
from .views.user_view import user_view
from .views.login_view import login_view
from .views.socketio_view import socketio_views
from .views.search_result_view import search_result_view

def create_app():
    app = Flask(__name__)
    socketio.init_app(app)
    app.config.from_object(Config)
    database_config(app)
    jwt = JWTManager(app)
    app.register_blueprint(user_view)
    app.register_blueprint(login_view)
    app.register_blueprint(csv_view)
    app.register_blueprint(search_result_view)
    app.register_blueprint(socketio_views)
    

    return app
