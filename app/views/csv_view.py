from flask import Blueprint, request, jsonify
from app.services.csv_service import CsvService
from flask_jwt_extended import jwt_required, get_jwt_identity

from app import socketio

csv_view = Blueprint('csv_view', __name__)

@csv_view.route('/csv', methods=['POST'])
@jwt_required()
def upload_csv():
    file = request.files['csv_file']
    if not file:
        return jsonify({'message': 'Error no CSV file'}), 400
    current_user = get_jwt_identity()
    csv = CsvService.processing_csv(file, current_user)
    socketio.emit('update_status', {'message': 'CSV processing initiated'})
    return jsonify({'message': csv})

@csv_view.route('/read-csv', methods=['GET'])
@jwt_required()
def read_all_csv():
    user_id = get_jwt_identity()
    files = CsvService.read_files(user_id)
    return jsonify(files)

@csv_view.route('/read-csv/<int:record_id>', methods=['GET'])
@jwt_required()
def read_csv(record_id):
    files = CsvService.read_file(record_id)
    return jsonify(files)


@socketio.on('search_csv')
def search_csv(record_id):
    print("record_id test", record_id)
    files = CsvService.handle_csv(record_id)
    return jsonify(files)