from urllib import response
from flask import Blueprint, request, jsonify
from app.services.login_service import LoginService
from flask_jwt_extended import get_jwt_identity, jwt_required

login_view = Blueprint('login_view', __name__)

@login_view.route('/login', methods=['POST'])
def login():
    data = request.get_json()
    response = LoginService.login(data)
    return jsonify(response)

@login_view.route('/refresh-token', methods=['GET'])
@jwt_required()
def refresh_token():
    user_id = get_jwt_identity()
    return LoginService.refresh_token(user_id)
