from flask import Blueprint, request, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity

from app.services.search_result_service import SearchResultService


search_result_view = Blueprint("search_result_view", __name__)


@search_result_view.route("/search-results", methods=["GET"])
@jwt_required()
def get_search_results():
    current_user = get_jwt_identity()
    search_results = SearchResultService.get_all_search_results(current_user)
    return jsonify(search_results)


@search_result_view.route("/search-results/<int:record_id>", methods=["GET"])
@jwt_required()
def get_search_result(record_id):
    search_results = SearchResultService.get_search_result(record_id)
    return jsonify(search_results)


@search_result_view.route("/search-results/html-code/<int:keyword_id>", methods=["GET"])
@jwt_required()
def get_html_code(keyword_id):
    search_results = SearchResultService.get_html_code(keyword_id)
    return jsonify(search_results)