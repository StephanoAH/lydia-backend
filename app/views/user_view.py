from flask import Blueprint, request, jsonify
from app.services.user_service import UserService
from flask_jwt_extended import jwt_required, get_jwt_identity

user_view = Blueprint('user_view', __name__)

@user_view.route('/users', methods=['GET'])
@jwt_required()
def get_users():
    users = UserService.get_users()
    return jsonify(users)

@user_view.route('/signup', methods=['POST'])
def add_user():
    data = request.get_json()
    new_user = UserService.add_user(data)
    return jsonify(new_user)

@user_view.route('/users/<int:user_id>', methods=['PUT'])
@jwt_required()
def update_user(user_id):
    current_user = get_jwt_identity()
    if current_user != user_id:
        return jsonify({'message': 'Unauthorized'}), 401

    data = request.get_json()
    response = UserService.update_user(user_id, data)
    return jsonify(response)

@user_view.route('/users/<int:user_id>', methods=['DELETE'])
@jwt_required()
def delete_user(user_id):
    current_user = get_jwt_identity()
    if current_user != user_id:
        return jsonify({'message': 'Unauthorized'}), 401
    
    response = UserService.delete_user(user_id)
    return jsonify(response)
