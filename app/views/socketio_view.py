from flask import Blueprint, render_template
from flask_socketio import emit, join_room, leave_room
from app.services.csv_service import CsvService
from app import socketio

socketio_views = Blueprint('socketio_views', __name__)

@socketio.on('connect')
def handle_connect():
    print('Client connected')
    emit('update_status', {'message': 'Connected to SocketIO'})

    return 'one'

@socketio.on('disconnect')
def handle_disconnect():
    print('Client disconnected')

@socketio.on('join_room')
def handle_join_room(data):
    room = data['room']
    join_room(room)
    print(f'Client joined room: {room}')

@socketio.on('leave_room')
def handle_leave_room(data):
    room = data['room']
    leave_room(room)
    print(f'Client left room: {room}')
