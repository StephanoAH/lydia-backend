import click
from flask import current_app
from flask.cli import with_appcontext


@click.command('initdb', help='Initialize the database.')
@with_appcontext
def init_db():
    print("Inside")
    with current_app.open_resource('scripts/lydia.sql', mode='r') as f:
        with current_app.app_context():
            with current_app.config['DB_POOL'].getconn() as conn:
                with conn.cursor() as cursor:
                    try:
                        cursor.execute(f.read())  
                    except Exception as e:
                        click.echo(f'Error initializing the database: {e}')
    print('Initialized the database.')
