import psycopg2
from psycopg2 import pool
from flask import g, current_app

def database_config(app):
    app.config['DB_POOL'] = pool.SimpleConnectionPool(
        1,  # Min connections
        100,  # Max connections
        dbname=app.config['DB_NAME'],
        user=app.config['DB_USER'],
        password=app.config['DB_PASSWORD'],
        host=app.config['DB_HOST'],
        port=app.config['DB_PORT']
    )

    @app.before_request
    def before_request():
        g.db = app.config['DB_POOL'].getconn()

    @app.teardown_request
    def teardown_request(exception=None):
        if hasattr(g, 'db'):
            app.config['DB_POOL'].putconn(g.db)

    @app.cli.command('initdb')
    def initdb_command():
        from app.database.init_db import init_db
        init_db(app)
        print('Initialized the database.')

    @app.cli.command('cleardb')
    def cleardb_command():
        from app.database.clear_db import clear_db
        clear_db(app)
        print('Cleared the database.')
