from flask import g

def get_cursor():
    return g.db.cursor()

def close_cursor(cursor):
    cursor.close()
    g.db.commit()
