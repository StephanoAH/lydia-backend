from datetime import datetime
from .cursor import get_cursor, close_cursor


def insert_search_result(total_adwords,	total_links, total_results, total_seconds,	html_code, keyword_id, record_id):
    try:
        cursor = get_cursor()
        cursor.execute("INSERT INTO search_results (total_adwords,	total_links,	total_results, total_seconds, html_code, keyword_id, record_id) VALUES (%s, %s, %s, %s, %s, %s, %s) RETURNING id", (total_adwords,	total_links, total_results, total_seconds,	html_code, keyword_id, record_id))
        search_result_id = cursor.fetchone()[0]
        close_cursor(cursor)
        return search_result_id
    except Exception as e:
        print(f"Error inserting search_results: {str(e)}")
        raise
