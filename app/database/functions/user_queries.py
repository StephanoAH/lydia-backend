from datetime import datetime
from .cursor import get_cursor, close_cursor


def create_user(username, encrypted_password):
    try:
        cursor = get_cursor()
        cursor.execute("INSERT INTO users (username, password) VALUES (%s, %s) RETURNING id", (username, encrypted_password))
        new_user_id = cursor.fetchone()[0]
        close_cursor(cursor)
        return new_user_id
    except Exception as e:
        print(f"Error inserting users: {str(e)}")
        raise


def read_all_users():
    try:
        cursor = get_cursor()
        cursor.execute("SELECT * FROM users")
        users = cursor.fetchall()
        close_cursor(cursor)
        return users
    except Exception as e:
        print(f"Error reading users: {str(e)}")
        raise


def update_user(username, encrypted_password, user_id):
    try:
        cursor = get_cursor()
        cursor.execute("UPDATE users SET username=%s, password=%s WHERE id=%s", (username, encrypted_password, user_id))
        close_cursor(cursor)
        return {'message': 'User updated successfully'}
    except Exception as e:
        print(f"Error updating users: {str(e)}")
        raise


def delete_user(user_id):
    try:
        cursor = get_cursor()
        cursor.execute("DELETE FROM users WHERE id=%s", (user_id,))
        close_cursor(cursor)
        return {'message': 'User deleted successfully'}
    except Exception as e:
        print(f"Error reading users: {str(e)}")
        raise