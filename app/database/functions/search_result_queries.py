from datetime import datetime

import psycopg2
from .cursor import get_cursor, close_cursor


def insert_search_result(
    total_adwords,
    total_links,
    total_results,
    total_seconds,
    html_code,
    keyword_id,
    record_id,
):
    try:
        connection = psycopg2.connect(dbname="lydia", user="cerbero", password="JkrqUP4N6Tn3cB2qXKNinLl4zo61gUZTJu5qCsN2J94wNa6QC6", host="144.91.124.50", port="12015")
        cursor = connection.cursor()
        cursor.execute(
            "INSERT INTO search_results (total_adwords,	total_links,	total_results, total_seconds, html_code, keyword_id, record_id) VALUES (%s, %s, %s, %s, %s, %s, %s) RETURNING id",
            (
                total_adwords,
                total_links,
                int(total_results),
                float(total_seconds),
                html_code,
                keyword_id,
                record_id,
            ),
        )
        cursor.close()
        connection.commit()
    except Exception as e:
        print(f"Error inserting search_results: {str(e)}")
        raise


def read_all_search_result(
    user_id,
):
    try:
        cursor = get_cursor()
        cursor.execute(
            """
          SELECT
            jsonb_build_object(
                'id', MAX(r.id),
                'filename', MAX(document_name),
                'upload_date', MAX(upload_date),
                'keywords', jsonb_agg(jsonb_build_object(
                    'search_result_id', sr.id,
                    'record_id', r.id,
                    'keyword', k.keyword,
                    'keyword_id', k.id,
                    'total_links', sr.total_links,
                    'total_adwords', sr.total_adwords,
                    'total_results', sr.total_results,
                    'total_seconds', ROUND(sr.total_seconds, 2)
                ))
            )
        FROM
            records r
        LEFT JOIN
            keywords k ON r.id = k.record_id
        LEFT JOIN
            public.search_results sr ON k.id = sr.keyword_id AND r.id = sr.record_id
        WHERE 
	        r.user_id = %s::integer
        GROUP BY
            r.id;
        """,
            (int(user_id),),
        )
        search_results = cursor.fetchall()
        result = [result[0] for result in search_results]
        close_cursor(cursor)
        return result
    except Exception as e:
        print(f"Error reading all search results: {str(e)}")
        raise


def read_search_result(
    record_id,
):
    try:
        cursor = get_cursor()
        cursor.execute(
            """
          SELECT
            jsonb_build_object(
                'id', MAX(r.id),
                'filename', MAX(document_name),
                'upload_date', MAX(upload_date),
                'keywords', jsonb_agg(jsonb_build_object(
                    'search_result_id', sr.id,
                    'record_id', r.id,
                    'keyword', k.keyword,
                    'keyword_id', k.id,
                    'total_links', sr.total_links,
                    'total_adwords', sr.total_adwords,
                    'total_results', sr.total_results,
                    'total_seconds', ROUND(sr.total_seconds, 2)
                ))
            )
        FROM
            records r
        LEFT JOIN
            keywords k ON r.id = k.record_id
        LEFT JOIN
            public.search_results sr ON k.id = sr.keyword_id AND r.id = sr.record_id
        WHERE
            r.id = %s::integer
        GROUP BY
    r.id;
        """,
            (int(record_id),),
        )
        result = cursor.fetchone()
        result_dict = result[0] if result else {}
        close_cursor(cursor)
        return result_dict
    except Exception as e:
        print(f"Error reading search result: {str(e)}")
        raise

def read_html_code(
    keyword_id,
):
    try:
        cursor = get_cursor()
        cursor.execute("SELECT html_code FROM public.search_results WHERE keyword_id =%s;", (keyword_id,))
        result = cursor.fetchone()[0]
        close_cursor(cursor)
        return result
    except Exception as e:
        print(f"Error reading search result: {str(e)}")
        raise