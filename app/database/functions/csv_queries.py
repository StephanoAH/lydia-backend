from datetime import datetime

from flask import g
import psycopg2
from .cursor import get_cursor, close_cursor


def insert_record(file_name, user_id):
    timestamp = datetime.now()
    try:
        cursor = get_cursor()
        cursor.execute(
            "INSERT INTO records (document_name, upload_date, user_id) VALUES (%s, %s, %s) RETURNING id",
            (file_name, timestamp, user_id),
        )
        record_id = cursor.fetchone()[0]
        close_cursor(cursor)
        return record_id
    except Exception as e:
        print(f"Error inserting records: {str(e)}")
        raise

def read_keywords(record_id):
    try:
        connection = psycopg2.connect(dbname="lydia", user="cerbero", password="JkrqUP4N6Tn3cB2qXKNinLl4zo61gUZTJu5qCsN2J94wNa6QC6", host="144.91.124.50", port="12015")
        cursor = connection.cursor()
        cursor.execute(
            "select * from keywords where record_id = %s::integer;",
            (int(record_id),),
        )
        result = cursor.fetchall()
        cursor.close()
        connection.commit()
        return result
    except Exception as e:
        print(f"Error reading keywords: {str(e)}")
        raise

def insert_keyword(keywords, record_id):
    try:
        cursor = get_cursor()
        cursor.execute(
            "INSERT INTO keywords (keyword, record_id) VALUES (%s, %s) RETURNING id",
            (keywords, record_id),
        )
        keyword_id = cursor.fetchone()[0]
        close_cursor(cursor)
        return keyword_id
    except Exception as e:
        print(f"Error inserting keywords: {str(e)}")
        raise


def read_all_files(user_id):
    try:
        cursor = get_cursor()
        cursor.execute(
            """
            SELECT
                jsonb_agg(
                    jsonb_build_object(
                        'id', r.id,
                        'document_name', r.document_name, 
                        'upload_date', r.upload_date
                    )
                )
            FROM
                records r
            WHERE
                r.user_id = %s; 
        """,
            (int(user_id),),
        )
        files = cursor.fetchall()
        [result] = [result[0] for result in files]
        close_cursor(cursor)
        return result
    except Exception as e:
        print(f"Error reading files: {str(e)}")
        raise


def read_file(record_id):
    try:
        cursor = get_cursor()
        cursor.execute(
            """
          SELECT
              jsonb_build_object(
                  'filename', MAX(document_name),
                  'upload_date', MAX(upload_date),
                  'id', MAX(r.id),
                  'keywords', string_agg(keyword, ', '),
                  'keyword_ids', array_agg(k.id),
                  'average_adwords', ROUND(AVG(sr.total_adwords), 0),
                  'average_links', ROUND(AVG(sr.total_links), 0),
                  'average_seconds', ROUND(AVG(sr.total_seconds), 2),
                  'average_total', ROUND(AVG(sr.total_results), 0)
              ) AS result
          FROM
              records r
          LEFT JOIN
              keywords k ON r.id = k.record_id
          LEFT JOIN
              public.search_results sr ON k.id = sr.keyword_id AND r.id = sr.record_id
          WHERE
              r.id = %s::integer;
        """,
            (int(record_id),),
        )
        result = cursor.fetchone()
        result_dict = result[0] if result else {}
        close_cursor(cursor)
        return result_dict
    except Exception as e:
        print(f"Error reading files: {str(e)}")
        raise
