import click
from flask import current_app
from flask.cli import with_appcontext
import psycopg2

@click.command('cleardb', help='Clear the database.')
@with_appcontext
def clear_db():
    with current_app.open_resource('scripts/clear.sql', mode='r') as f:
        with current_app.app_context():
            with current_app.config['DB_POOL'].getconn() as conn:
                with conn.cursor() as cursor:
                    cursor.execute(f.read())
    print('Cleared the database.')
