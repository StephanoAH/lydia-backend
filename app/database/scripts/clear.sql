-- Drop foreign key constraints
ALTER TABLE search_results DROP CONSTRAINT IF EXISTS fk_search_result_keyword_record;
ALTER TABLE keywords DROP CONSTRAINT IF EXISTS fk_keyword_record;
ALTER TABLE records DROP CONSTRAINT IF EXISTS fk_record_user;

-- Drop tables in reverse order to avoid foreign key constraint issues
DROP TABLE IF EXISTS search_results;
DROP TABLE IF EXISTS keywords;
DROP TABLE IF EXISTS records;
DROP TABLE IF EXISTS users;

-- Drop the database
DROP DATABASE IF EXISTS lydia;
