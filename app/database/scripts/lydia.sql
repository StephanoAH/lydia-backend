-- Create Database
CREATE DATABASE lydia;

-- -----------------------------------------------------
-- Table user
-- -----------------------------------------------------
CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  username VARCHAR(45) NOT NULL,
  first_name VARCHAR(45) NOT NULL,
  last_name VARCHAR(45) NOT NULL,
  email VARCHAR(45) NOT NULL,
  password VARCHAR(45) NOT NULL,
  CONSTRAINT username_unique UNIQUE (username),
  CONSTRAINT email_unique UNIQUE (email)
);

-- -----------------------------------------------------
-- Table record
-- -----------------------------------------------------
CREATE TABLE public.users (
	id serial4 NOT NULL,
	username varchar(45) NOT NULL,
	"password" varchar(255) NOT NULL,
	CONSTRAINT username_unique UNIQUE (username),
	CONSTRAINT users_pkey PRIMARY KEY (id)
);

-- -----------------------------------------------------
-- Table keyword
-- -----------------------------------------------------
CREATE TABLE keywords (
  id SERIAL NOT NULL,
	total_adwords INT NOT NULL,
	total_links INT NOT NULL,
	total_results VARCHAR NOT NULL,
	html_code text NOT NULL,
	keyword_id INT NOT NULL,
	record_id INT NOT NULL,
	total_seconds VARCHAR NULL,
  PRIMARY KEY (id, record_id), -- Use PRIMARY KEY constraint for uniqueness
  CONSTRAINT fk_keyword_record FOREIGN KEY (record_id) REFERENCES records(id) ON DELETE CASCADE ON UPDATE CASCADE
);

-- -----------------------------------------------------
-- Table search_result
-- -----------------------------------------------------
CREATE TABLE search_results (
  id SERIAL PRIMARY KEY,
  total_adwords INT NOT NULL,
  total_links INT NOT NULL,
  total_results DOUBLE PRECISION NOT NULL,
  html_code TEXT NOT NULL,
  keyword_id INT NOT NULL,
  record_id INT NOT NULL,
  CONSTRAINT fk_search_result_keyword_record FOREIGN KEY (keyword_id, record_id) REFERENCES keywords(id, record_id) ON DELETE CASCADE ON UPDATE CASCADE
);