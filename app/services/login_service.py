from flask import g, jsonify
from flask_jwt_extended import create_access_token
from werkzeug.security import check_password_hash

from ..config import Config

class LoginService:
    @staticmethod
    def login(data):
            username = data['username']
            password = data['password']

            with g.db.cursor() as cursor:
                cursor.execute("SELECT * FROM users WHERE username=%s", (username,))
                user = cursor.fetchone()

            user_id, *user_data, encryptedPassword = user
            if check_password_hash(encryptedPassword, password):
                access_token = create_access_token(identity=user_id)
                return {'access_token': access_token}
            else:
                return {'message': 'Invalid credentials'}

    @staticmethod
    def refresh_token(user_id):
        access_token = create_access_token(identity=user_id)
        return {'access_token': access_token}