from app.database.functions.user_queries import create_user, delete_user, read_all_users, update_user
from app.models.user_model import UserModel
from werkzeug.security import generate_password_hash


class UserService:
    @staticmethod
    def get_users():
        users = read_all_users()
        user_objects = [UserModel(*user) for user in users]
        user_list = [{'id': user.id, 'username': user.username} for user in user_objects]

        return user_list


    @staticmethod
    def add_user(data):
        username = data['username']
        password = data['password']
        encrypted_password = generate_password_hash(password)

        new_user_id = create_user(username, encrypted_password)

        return {'id': new_user_id, 'username': username}


    @staticmethod
    def update_user(user_id, data):
        username = data['username']
        password = data['password']
        encrypted_password = generate_password_hash(password)

        updated_user = update_user(username, encrypted_password, user_id)

        return updated_user


    @staticmethod
    def delete_user(user_id):
        deleted_user = delete_user(user_id)

        return deleted_user
