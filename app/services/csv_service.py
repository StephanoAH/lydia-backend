import csv
from io import StringIO
from flask import g, jsonify
from flask_socketio import emit
from app import socketio_config

from app.database.functions.csv_queries import (
    insert_keyword,
    insert_record,
    read_all_files,
    read_file,
    read_keywords,
)
from app.services.aracne import scrape
from app import socketio

class CsvService:
    @staticmethod
    def processing_csv(file, user_id):
        try:
            stream = StringIO(file.stream.read().decode("UTF-8"))
            csv_reader = csv.reader(stream)
            [keywords] = [row for row in csv_reader]
            record = insert_record(file.filename, user_id)
            for keyword in keywords:
                fixedKeyword = keyword.replace(" ", "")
                insert_keyword(fixedKeyword, record)

            socketio.emit('update_status', {'message': 'Scraping completed!'})
            return record
        except Exception as e:
            print(f"Error processing csv: {str(e)}")
            raise

    @staticmethod
    def read_files(user_id):
        try:
            files = read_all_files(user_id)
            return files
        except Exception as e:
            print(f"Error reading files: {str(e)}")
            raise

    @staticmethod
    def read_file(record_id):
        try:
            file = read_file(record_id)
            return file
        except Exception as e:
            print(f"Error reading files: {str(e)}")
            raise

    @staticmethod
    def handle_csv(record_id):
        print(f'Client test: {record_id}')
        keywords = read_keywords(record_id)
        print("keyword", keywords)
        for keyword in keywords:
            scrape(keyword[2], keyword[1], keyword[0])
        emit('update_status', {'message': 'Connected to SocketIO'})