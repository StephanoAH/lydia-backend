from app.database.functions.search_result_queries import (
    read_all_search_result,
    read_html_code,
    read_search_result,
)


class SearchResultService:
    @staticmethod
    def get_all_search_results(user_id):
        try:
            search_results = read_all_search_result(user_id)
            return search_results
        except Exception as e:
            print(f"Error reading all search results: {str(e)}")
            raise

    @staticmethod
    def get_search_result(record_id):
        try:
            search_result = read_search_result(record_id)
            return search_result
        except Exception as e:
            print(f"Error reading search result: {str(e)}")
            raise

    @staticmethod
    def get_html_code(keyword_id):
        try:
            html_code = read_html_code(keyword_id)
            return html_code
        except Exception as e:
            print(f"Error reading search result: {str(e)}")
            raise
