from app import socketio
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException

from app.database.functions.search_result_queries import insert_search_result


def scrape(keyword, record_id, keyword_id):
    driver = None

    try:
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        driver = webdriver.Chrome(
            service=Service(ChromeDriverManager().install()), options=options
        )

        # Open Google
        driver.get("https://www.google.com/search?q=" + keyword)

        # Wait for the results to load
        WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "result-stats"))
        )

        # Extract information
        ads_count = len(driver.find_elements(By.CLASS_NAME, "v5yQqb"))
        links_count = len(driver.find_elements(By.TAG_NAME, "a"))

        # Extract the total search results using XPath
        results_seconds = (
            driver.find_element(By.XPATH, "//div[@id='result-stats']")
            .text.split("(")[1]
            .split(" ")[0]
            .replace(",", ".")
        )
        results_total = (
            driver.find_element(By.XPATH, "//div[@id='result-stats']")
            .text.split()[1]
            .replace(".", "")
        )

        # Extract HTML code of the page
        html_code = str(driver.page_source)

        insert_search_result(
            ads_count,
            links_count,
            results_total,
            results_seconds,
            html_code,
            keyword_id,
            record_id,
        )

    except TimeoutException:
        print("Timeout waiting for the results page to load.")
        socketio.emit('update_status', {'message': 'Timeout waiting for the results page to load.', 'keyword': keyword})

    except NoSuchElementException:
        print("Element not found. Check if the page structure has changed.")
        socketio.emit('update_status', {'message': 'Element not found. Check if the page structure has changed.', 'keyword': keyword})

    except Exception as e:
        print(f"An unexpected error occurred: {e}")
        socketio.emit('update_status', {'message': f'An unexpected error occurred: {e}', 'keyword': keyword})

    finally:
        if driver is not None:
            driver.quit()
